<?php
// $Id$

/**
 * @file
 * Admin functions for fbtwgooglebuttons.
 */

/**
 * Node Settings.
 */
function fbtwgoogle_admin_settings() {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General options'),
  );
  $options = array('1' => t('Enabled'), '0' => t('Disabled'));
  $form['settings']['fbtwgoogle_container'] = array(
    '#type' => 'select',
    '#title' => t('Set the social buttons container'),
    '#options' => array(t('none'),t('p'), t('div')),
    '#default_value' => variable_get('fbtwgoogle_container', 2),
    '#description' => t('the container is the html element where all the buttons will be included.'),
  );
  $form['settings']['fbtwgoogle_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra css styling needed for the container'),
    '#default_value' => variable_get('fbtwgoogle_css', ''),
    '#description' => t('Extra css attributes needed to make the containe. Example: <em>float: right; padding: 5px;</em>'),
  );
  $form['settings']['fbtwgoogle_button_container'] = array(
    '#type' => 'select',
    '#title' => t('Set the button container'),
    '#options' => array(t('none'),t('p'), t('div')),
    '#default_value' => variable_get('fbtwgoogle_button_container', 2),
    '#description' => t('the container is the html element where the single button will be included.'),
  );
  $form['settings']['fbtwgoogle_buttons_weight'] = array(
    '#type' => 'select',
    '#title' => t('Buttons Weight'),
    '#options' => array('-50' => '-50', '-49' => '-49', '-48' => '-48', '-47' => '-47', '-46' => '-46', '-45' => '-45', '-44' => '-44', '-43' => '-43', '-42' => '-42', '-41' => '-41', '-40' => '-40', '-39' => '-39', '-38' => '-38', '-37' => '-37', '-36' => '-36', '-35' => '-35', '-34' => '-34', '-33' => '-33', '-32' => '-32', '-31' => '-31', '-30' => '-30', '-29' => '-29', '-28' => '-28', '-27' => '-27', '-26' => '-26', '-25' => '-25', '-24' => '-24', '-23' => '-23', '-22' => '-22', '-21' => '-21', '-20' => '-20', '-19' => '-19', '-18' => '-18', '-17' => '-17', '-16' => '-16', '-15' => '-15', '-14' => '-14', '-13' => '-13', '-12' => '-12', '-11' => '-11', '-10' => '-10', '-9' => '-9', '-8' => '-8', '-7' => '-7', '-6' => '-6', '-5' => '-5', '-4' => '-4', '-3' => '-3', '-2' => '-2', '-1' => '-1', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '41' => '41', '42' => '42', '43' => '43', '44' => '44', '45' => '45', '46' => '46', '47' => '47', '48' => '48', '49' => '49', '50' => '50'),    
    '#default_value' => variable_get('fbtwgoogle_buttons_weight', '49'),
    '#description' => t('The weight determines where on a node the social button will appear. The larger the weight, the lower it will appear on the node. For example, if you want the button to appear more toward the top of the node, choose <em>-40</em> as opposed to <em>-39, -38, 0, 1,</em> or <em>50,</em> etc.'),
  );
  $form['settings']['fbtwgoogle_comments_weight'] = array(
    '#type' => 'select',
    '#title' => t('Comments Weight'),
    '#options' => array('-50' => '-50', '-49' => '-49', '-48' => '-48', '-47' => '-47', '-46' => '-46', '-45' => '-45', '-44' => '-44', '-43' => '-43', '-42' => '-42', '-41' => '-41', '-40' => '-40', '-39' => '-39', '-38' => '-38', '-37' => '-37', '-36' => '-36', '-35' => '-35', '-34' => '-34', '-33' => '-33', '-32' => '-32', '-31' => '-31', '-30' => '-30', '-29' => '-29', '-28' => '-28', '-27' => '-27', '-26' => '-26', '-25' => '-25', '-24' => '-24', '-23' => '-23', '-22' => '-22', '-21' => '-21', '-20' => '-20', '-19' => '-19', '-18' => '-18', '-17' => '-17', '-16' => '-16', '-15' => '-15', '-14' => '-14', '-13' => '-13', '-12' => '-12', '-11' => '-11', '-10' => '-10', '-9' => '-9', '-8' => '-8', '-7' => '-7', '-6' => '-6', '-5' => '-5', '-4' => '-4', '-3' => '-3', '-2' => '-2', '-1' => '-1', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '41' => '41', '42' => '42', '43' => '43', '44' => '44', '45' => '45', '46' => '46', '47' => '47', '48' => '48', '49' => '49', '50' => '50'),    
    '#default_value' => variable_get('fbtwgoogle_comments_weight', '50'),
    '#description' => t('The weight determines where on a node the social button will appear. The larger the weight, the lower it will appear on the node. For example, if you want the button to appear more toward the top of the node, choose <em>-40</em> as opposed to <em>-39, -38, 0, 1,</em> or <em>50,</em> etc.'),
  );
  $form['enable'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable options'),
  );
  $form['enable']['fbtwgoogle_enable_fb_like'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Facebook Like button'),
    '#options' => $options,
    '#default_value' => variable_get('fbtwgoogle_enable_fb_like', 1),
    '#description' => t('Enable the Facebook Like button on the node content.'),
  );
  $form['enable']['fbtwgoogle_enable_fb_share'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Facebook Share button'),
    '#options' => $options,
    '#default_value' => variable_get('fbtwgoogle_enable_fb_share', 1),
    '#description' => t('Enable the Facebook Share button on the node content.'),
  );
  $form['enable']['fbtwgoogle_enable_fb_comments'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Facebook Comments'),
    '#options' => $options,
    '#default_value' => variable_get('fbtwgoogle_enable_fb_comments', 1),
    '#description' => t('Enable the Facebook Comments Plugin on the node content.'),
  );
  $form['enable']['fbtwgoogle_enable_tw'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Twitter button'),
    '#options' => $options,
    '#default_value' => variable_get('fbtwgoogle_enable_tw', 1),
    '#description' => t('Enable the Twitter button on the node content.'),
  );
  $form['enable']['fbtwgoogle_enable_google'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Google +1 button'),
    '#options' => $options,
    '#default_value' => variable_get('fbtwgoogle_enable_google', 1),
    '#description' => t('Enable the Google +1 button on the node content.'),
  );
  $form['order'] = array(
    '#type' => 'fieldset',
    '#title' => t('Position order options'),
  );
  $form['order']['fbtwgoogle_weight_fb_like'] = array(
    '#type' => 'select',
    '#title' => t('Facebook Like Button Weight'),
    '#options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),    
    '#default_value' => variable_get('fbtwgoogle_weight_fb_like', '1'),
    '#description' => t('The weight for the position order.'),
  );
  $form['order']['fbtwgoogle_weight_fb_share'] = array(
    '#type' => 'select',
    '#title' => t('Facebook Share Button Weight'),
    '#options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),    
    '#default_value' => variable_get('fbtwgoogle_weight_fb_share', '4'),
    '#description' => t('The weight for the position order.'),
  );
  $form['order']['fbtwgoogle_weight_tw'] = array(
    '#type' => 'select',
    '#title' => t('Facebook Twitter Button Weight'),
    '#options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),    
    '#default_value' => variable_get('fbtwgoogle_weight_tw', '2'),
    '#description' => t('The weight for the position order.'),
  );
  $form['order']['fbtwgoogle_weight_google'] = array(
    '#type' => 'select',
    '#title' => t('Facebook Google +1 Button Weight'),
    '#options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),    
    '#default_value' => variable_get('fbtwgoogle_weight_google', '3'),
    '#description' => t('The weight for the position order.'),
  );

  return system_settings_form($form);
}


function fb_admin_settings() {
  $fb_node_options = node_type_get_names();
  $form['fb_share'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Settings for the Facebook Share Button'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fb_share']['fb_share_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Share button on these content types:'),
    '#options' => $fb_node_options,
    '#default_value' => variable_get('fb_share_node_types', array('article')),
    '#description' => t('Each of these content types will have the "share" button automatically added to them.'),
  );
  $form['fb_share']['fb_share_showonteasers'] = array(
    '#type' => 'select',
    '#title' => t('Show the Share button on teasers?'),
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('fb_share_showonteasers', 0),
    '#description' => t('If <em>Yes</em> is selected, the button will appear even when the node being viewed is a teaser. Otherwise it will only appear when the full node is being viewed.'),
  );
  $form['fb_share']['fb_share_layout'] = array(
    '#type' => 'select',
    '#title' => t('Layout style'),
    '#options' => array('icontext' => t('Icontext'), 'button_count' => t('Button Count'), 'box_count' => t('Box Count'), 'text' => t('Text'), 'icon' => t('Icon')),
    '#default_value' => variable_get('fb_share_layout', 'standard'),
    '#description' => t('Determines the layout type for the button.'),
  );
  $form['fb_share']['fb_share_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra css styling needed for the container'),
    '#default_value' => variable_get('fb_share_css', 'display:inline-block;vertical-align:top;margin:5px;'),
    '#description' => t('Extra css attributes needed to make the containe. Example: <em>float: right; padding: 5px;</em>'),
  );
  
  $form['fb_comments'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Settings for the Facebook Comments'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fb_comments']['fb_comments_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Facebook Comments on these content types:'),
    '#options' => $fb_node_options,
    '#default_value' => variable_get('fb_comments_node_types', array('article')),
    '#description' => t('Each of these content types will have the Facebook Comments automatically added to them.'),
  );
  $form['fb_comments']['fb_comments_showonteasers'] = array(
    '#type' => 'select',
    '#title' => t('Show the Facebook Comments on teasers?'),
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('fb_comments_showonteasers', 0),
    '#description' => t('If <em>Yes</em> is selected, the Facebook Comments will appear even when the node being viewed is a teaser. Otherwise it will only appear when the full node is being viewed.'),
  );
  $form['fb_comments']['fb_comments_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra css styling needed for the container'),
    '#default_value' => variable_get('fb_comments_css', 'display:inline-block;vertical-align:middle;margin:5px;'),
    '#description' => t('Extra css attributes needed to make the container. Example: <em>float: right; padding: 5px;</em>'),
  );
  $form['fb_comments']['fb_comments_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width (px)'),
    '#default_value' => variable_get('fb_comments_width', '500'),
    '#description' => t('Width of the Comments Box, in pixels. Default is 500. <em>Note: lower values may crop the output.</em>'),
  );
  $form['fb_comments']['fb_comments_color_scheme'] = array(
    '#type' => 'select',
    '#title' => t('Color scheme'),
    '#options' => array('light' => t('Light'), 'dark' => t('Dark')),
    '#default_value' => variable_get('fb_comments_color_scheme', 'light'),
    '#description' => t('The color scheme of the plugin.'),
  );
  $form['fb_comments']['fb_comments_num_posts'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of post'),
    '#default_value' => variable_get('fb_comments_num_posts', '5'),
    '#description' => t('Width of the Number of post in Comments.'),
  );


  $form['fb_like'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Settings for the Facebook Like Button'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fb_like']['fb_like_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Like button on these content types:'),
    '#options' => $fb_node_options,
    '#default_value' => variable_get('fb_like_node_types', array('article')),
    '#description' => t('Each of these content types will have the "like" button automatically added to them.'),
  );
  $form['fb_like']['fb_like_showonteasers'] = array(
    '#type' => 'select',
    '#title' => t('Show the Like button on teasers?'),
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('fb_like_showonteasers', 0),
    '#description' => t('If <em>Yes</em> is selected, the button will appear even when the node being viewed is a teaser. Otherwise it will only appear when the full node is being viewed.'),
  );
  $form['fb_like']['fb_like_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra css styling needed for the container'),
    '#default_value' => variable_get('fb_like_css', 'display:inline-block;vertical-align:middle;margin:5px;'),
    '#description' => t('Extra css attributes needed to make the container. Example: <em>float: right; padding: 5px;</em>'),
  );
  $form['fb_like']['fb_like_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width of the iframe for the Like button (px)'),
    '#default_value' => variable_get('fb_like_width', '350'),
    '#description' => t('Width of the iframe, in pixels. Default is 350. <em>Note: lower values may crop the output.</em>'),
  );
  $form['fb_like']['fb_like_layout'] = array(
    '#type' => 'select',
    '#title' => t('Layout style'),
    '#options' => array('standard' => t('Standard'), 'box_count' => t('Box Count'), 'button_count' => t('Button Count')),
    '#default_value' => variable_get('fb_like_layout', 'standard'),
    '#description' => t('Determines the layout type for the button.'),
  );
  $form['fb_like']['fb_like_show_faces'] = array(
    '#type' => 'select',
    '#title' => t('Show faces in the box?'),
    '#options' => array('show' => t('Show faces'), 'hide' => t('Do not show faces')),
    '#default_value' => variable_get('fb_like_show_faces', 'show'),
    '#description' => t('Show profile pictures below the button. Only works if <em>Layout style</em> (found above) is set to <em>Standard</em> (otherwise, value is ignored).'),
  );
  $form['fb_like']['fb_like_verb'] = array(
    '#type' => 'select',
    '#title' => t('Verb to display'),
    '#options' => array('like' => t('Like'), 'recommend' => t('Recommend')),
    '#default_value' => variable_get('fb_like_verb', 'like'),
    '#description' => t('The verbe to display inside the button itself.'),
  );
  $form['fb_like']['fb_like_displaysend'] = array(
    '#type' => 'select',
    '#title' => t('Display <em>send</em> option?'),
    '#options' => array('true' => t('Display'), 'false' => t('Do not display')),
    '#default_value' => variable_get('fb_like_displaysend', 'false'),
    '#description' => t('Optionally display the <em>Send</em> button next to the <em>Like/Recommend</em> box.'),
  ); 
  $form['fb_like']['fb_like_font'] = array(
    '#type' => 'select',
    '#title' => t('Font'),
    '#options' => array('arial' => 'Arial',
                'lucida+grande' => 'Lucida Grande',
                'segoe+ui' => 'Segoe UI',
                'tahoma' => 'Tahoma',
                'trebuchet+ms' => 'Trebuchet MS',
                'verdana' => 'Verdana'),
    '#default_value' => variable_get('fb_like_font', 'arial'),
    '#description' => t('The font with which to display the text of the button.'),
  );
  $form['fb_like']['fb_like_color_scheme'] = array(
    '#type' => 'select',
    '#title' => t('Color scheme'),
    '#options' => array('light' => t('Light'), 'dark' => t('Dark')),
    '#default_value' => variable_get('fb_like_color_scheme', 'light'),
    '#description' => t('The color scheme of the button.'),
  );
  $form['fb_like']['fb_like_language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => array('ca_ES' => t('Catalan'),'cs_CZ' => t('Czech'),'cy_GB' => t('Welsh'),'da_DK' => t('Danish'),'de_DE' => t('German'),'eu_ES' => t('Basque'),'en_PI' => t('English - Pirate'),'en_UD' => t('English - Upside Down'),'ck_US' => t('Cherokee'),'en_US' => t('English - US'),'es_LA' => t('Spanish'),'es_CL' => t('Spanish - Chile'),'es_CO' => t('Spanish - Colombia'),'es_ES' => t('Spanish - Spain'),'es_MX' => t('Spanish - Mexico'),'es_VE' => t('Spanish - Venezuela'),'fb_FI' => t('Finnish - test'),'fi_FI' => t('Finnish'),'fr_FR' => t('French - France'),'gl_ES' => t('Galician'),'hu_HU' => t('Hungarian'),'it_IT' => t('Italian'),'ja_JP' => t('Japanese'),'ko_KR' => t('Korean'),'nb_NO' => t('Norwegian - bokmal'),'nn_NO' => t('Norwegian - nynorsk'),'nl_NL' => t('Dutch'),'pl_PL' => t('Polish'),'pt_BR' => t('Portuguese - Brazil'),'pt_PT' => t('Portuguese - Portugal'),'ro_RO' => t('Romanian'),'ru_RU' => t('Russian'),'sk_SK' => t('Slovak'),'sl_SI' => t('Slovenian'),'sv_SE' => t('Swedish'),'th_TH' => t('Thai'),'tr_TR' => t('Turkish'),'ku_TR' => t('Kurdish'),'zh_CN' => t('Simplified Chinese - China'),'zh_HK' => t('Traditional Chinese - Hong Kong'),'zh_TW' => t('Traditional Chinese - Taiwan'),'fb_LT' => t('Leet Speak'),'af_ZA' => t('Afrikaans'),'sq_AL' => t('Albanian'),'hy_AM' => t('Armenian'),'az_AZ' => t('Azeri'),'be_BY' => t('Belarusian'),'bn_IN' => t('Bengali'),'bs_BA' => t('Bosnian'),'bg_BG' => t('Bulgarian'),'hr_HR' => t('Croatian'),'nl_BE' => t('Dutch - Belgium'),'en_GB' => t('English - UK'),'eo_EO' => t('Esperanto'),'et_EE' => t('Estonian'),'fo_FO' => t('Faroese'),'fr_CA' => t('French - Canada'),'ka_GE' => t('Georgian'),'el_GR' => t('Greek'),'gu_IN' => t('Gujarati'),'hi_IN' => t('Hindi'),'is_IS' => t('Icelandic'),'id_ID' => t('Indonesian'),'ga_IE' => t('Irish'),'jv_ID' => t('Javanese'),'kn_IN' => t('Kannada'),'kk_KZ' => t('Kazakh'),'la_VA' => t('Latin'),'lv_LV' => t('Latvian'),'li_NL' => t('Limburgish'),'lt_LT' => t('Lithuanian'),'mk_MK' => t('Macedonian'),'mg_MG' => t('Malagasy'),'ms_MY' => t('Malay'),'mt_MT' => t('Maltese'),'mr_IN' => t('Marathi'),'mn_MN' => t('Mongolian'),'ne_NP' => t('Nepali'),'pa_IN' => t('Punjabi'),'rm_CH' => t('Romansh'),'sa_IN' => t('Sanskrit'),'sr_RS' => t('Serbian'),'so_SO' => t('Somali'),'sw_KE' => t('Swahili'),'tl_PH' => t('Filipino'),'ta_IN' => t('Tamil'),'tt_RU' => t('Tatar'),'te_IN' => t('Telugu'),'ml_IN' => t('Malayalam'),'uk_UA' => t('Ukrainian'),'uz_UZ' => t('Uzbek'),'vi_VN' => t('Vietnamese'),'xh_ZA' => t('Xhosa'),'zu_ZA' => t('Zulu'),'km_KH' => t('Khmer'),'tg_TJ' => t('Tajik'),'ar_AR' => t('Arabic'),'he_IL' => t('Hebrew'),'ur_PK' => t('Urdu'),'fa_IR' => t('Persian'),'sy_SY' => t('Syriac'),'yi_DE' => t('Yiddish'),'gn_PY' => t('Guarani'),'qu_PE' => t('Quechua'),'ay_BO' => t('Aymara'),'se_NO' => t('Northern Sami'),'ps_AF' => t('Pashto'),'tl_ST' => t('Klingon')),
    '#default_value' => variable_get('fb_like_language', 'en_US'),
    '#description' => t('Specific language to use. Default is English.'),
  );
  $form['fb_admin'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Facebook Administration Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fb_admin']['fb_adminsid'] = array(
    '#type' => 'textfield',
    '#title' => t('Admins ID'),
    '#default_value' => variable_get('fb_adminsid', ''),
    '#description' => t('The admins ID, type the ID of very admin separated by comma without spaces'),
  );
  $form['fb_admin']['fb_appid'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#default_value' => variable_get('fb_appid', ''),
    '#description' => t('The Facebook application ID.'),
  );
  $form['fb_admin']['picture'] = array('#type' => 'fieldset', '#title' => t('Fallback image'));
  $form['fb_admin']['picture']['fb_fallback_img'] = array('#type' => 'textfield', 
                                               '#title' => t('Fallback image'), 
                                               '#default_value' => variable_get('fb_fallback_img'),
                                               '#size' => 60, 
                                               '#maxlength' => 250,
		                                           '#description' => t('Type the url for the Facebook Opengraph fallback image path, relative to Drupal root directory. No leading slashes.')
                                              );
  $form['fb_admin']['picture']['fb_fallback_img_upload'] = array('#type' => 'file', 
                                               '#title' => t('Fallback image upload'), 
                                               '#size' => 60, 
                                               '#maxlength' => 250,
		                                           '#description' => t('Image that rappresent the website')
                                              );
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form['#validate'][] = '_fb_like_admin_settings_validate';
  return system_settings_form($form);
}



/**
 * Validate print_main_settings form.
 */
function _fb_like_admin_settings_validate($form, &$form_state) {
  if ($file = file_save_upload('fb_fallback_img_upload', array('file_validate_is_image' => array()),'public://')) {
    if (!$file) {
      drupal_set_message('Error uploading file.');
      return;
    }
    $form_state['values']['fb_fallback_img'] = file_create_url($file->uri);
  }
}





function tw_admin_settings() {
  $tw_node_options = node_type_get_names();
  $form['tw_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Twitter button on these content types:'),
    '#options' => $tw_node_options,
    '#default_value' => variable_get('tw_node_types', array('article')),
    '#description' => t('Each of these content types will have the "Twitter" button automatically added to them.'),
  );
  $form['tw_showonteasers'] = array(
    '#type' => 'select',
    '#title' => t('Show the Twitter button on teasers?'),
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('tw_showonteasers', 0),
    '#description' => t('If <em>Yes</em> is selected, the button will appear even when the node being viewed is a teaser. Otherwise it will only appear when the full node is being viewed.'),
  );
  $form['tw_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra css styling needed for the container'),
    '#default_value' => variable_get('tw_css', 'display:inline-block;vertical-align:middle;margin:5px;'),
    '#description' => t('Extra css attributes needed to make the container. Example: <em>float: right; padding: 5px;</em>'),
  );
  $form['tw_data_via'] = array(
    '#type' => 'textfield',
    '#title' => t('Your twitter account username'),
    '#default_value' => variable_get('tw_data_via', ''),
    '#description' => t('Insert the screen name of the user to attribute the Tweet to. This tells twitter who was the original tweeter. Make sure you change it to your twitter account!!'),
  );
  $form['tw_data_related'] = array(
    '#type' => 'textfield',
    '#title' => t('Related accounts'),
    '#default_value' => variable_get('tw_data_related', ''),
    '#description' => t('Insert the related accounts. This adds recommended users to follow. You are allowed up to two Twitter accounts for users to follow after they share content from your website. These accounts could include your own, or that of a contributor or a partner. The first account is the one that is shared in data-via property. (Make sure you change it to one of your other twitter accounts, or remove it). The correct format to enter data in this variable is <em>twitterusername:Description of the User</em>.'),
  );
  $form['tw_show_counter'] = array(
    '#type' => 'select',
    '#title' => t('Show counter for the Twitter button'),
    '#options' => array('vertical' => t('Vertical'),'horizontal' => t('Horizontal'),'none' => t('None')),
    '#default_value' => variable_get('tw_show_counter', 'horizontal'),
    '#description' => t('Show counter for the Twitter button'),
  );
  $form['tw_language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => array('ko' => t('Korean'),'fr' => t('French'),'ja' => t('Japanese'),'it' => t('Italian'),'id' => t('Indonesian'),'en' => t('English'),'nl' => t('Dutch'),'pt' => t('Portuguese'),'ru' => t('Russian'),'es' => t('Spanish'),'de' => t('German'),'tr' => t('Turkish')),
    '#default_value' => variable_get('tw_language', 'en'),
    '#description' => t('Specific language to use. Default is English.'),
  );
  return system_settings_form($form);
}







function google_admin_settings() {
  $google_node_options = node_type_get_names();
  $form['google_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Google +1 button on these content types:'),
    '#options' => $google_node_options,
    '#default_value' => variable_get('google_node_types', array('article')),
    '#description' => t('Each of these content types will have the "Google +1" button automatically added to them.'),
  );
  $form['google_showonteasers'] = array(
    '#type' => 'select',
    '#title' => t('Show the Google +1 button on teasers?'),
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('google_showonteasers', 0),
    '#description' => t('If <em>Yes</em> is selected, the button will appear even when the node being viewed is a teaser. Otherwise it will only appear when the full node is being viewed.'),
  );
  $form['google_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra css styling needed for the container'),
    '#default_value' => variable_get('google_css', 'display:inline-block;vertical-align:middle;margin:5px;'),
    '#description' => t('Extra css attributes needed to make the container. Example: <em>float: right; padding: 5px;</em>'),
  );
  $form['google_size'] = array(
    '#type' => 'select',
    '#title' => t('Set the size for the Google +1 button'),
    '#options' => array('small' => t('Small'),'medium' => t('Medium'),'standard' => t('Standard'),'tall' => t('Tall')),
    '#default_value' => variable_get('google_size', 'medium'),
    '#description' => t('Set the size for the Google +1 button'),
  );
  $form['google_show_counter'] = array(
    '#type' => 'select',
    '#title' => t('Show counter for the Google +1 button'),
    '#options' => array('1' => t('True'),'0' => t('False')),
    '#default_value' => variable_get('google_show_counter', '0'),
    '#description' => t('Show counter for the Twitter button'),
  );
  $form['google_language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => array('ar' => t('Arabic'),'bg' => t('Bulgarian'),'ca' => t('Catalan'),'zh-CN' => t('Chinese - Simplified'),'zh-TW' => t('Chinese - Traditional'),'hr' => t('Croatian'),'cs' => t('Czech'),'da' => t('Danish'),'nl' => t('Dutch'),'en-GB' => t('English - UK'),'en-US' => t('English - US'),'et' => t('Estonian'),'fil' => t('Filipino'),'fi' => t('Finnish'),'fr' => t('French'),'de' => t('German'),'el' => t('Greek'),'iw' => t('Hebrew'),'hi' => t('Hindi'),'hu' => t('Hungarian'),'id' => t('Indonesian'),'it' => t('Italian'),'ja' => t('Japanese'),'ko' => t('Korean'),'lv' => t('Latvian'),'lt' => t('Lithuanian'),'ms' => t('Malay'),'no' => t('Norwegian'),'fa' => t('Persian'),'pl' => t('Polish'),'pt-BR' => t('Portuguese - Brazil'),'pt-PT' => t('Portuguese - Portugal'),'ro' => t('Romanian'),'ru' => t('Russian'),'sr' => t('Serbian'),'sk' => t('Slovak'),'sl' => t('Slovenian'),'es' => t('Spanish'),'es-419' => t('Spanish - Latin America'),'sv' => t('Swedish'),'th' => t('Thai'),'tr' => t('Turkish'),'uk' => t('Ukrainian'),'vi' => t('Vietnamese')),
    '#default_value' => variable_get('google_language', 'en-US'),
    '#description' => t('Specific language to use. Default is English.'),
  );
  return system_settings_form($form);
 }
 

/*
function fbtwgooglebuttons_block_settings() {
  global $base_url;
  $form['fb_block_url'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('fb_block_url', $base_url),
    '#description' => t('URL of your homepage to like')
  );
  $form['fb_block'] = array(
    '#type' => 'fieldset',
    '#title' => 'Block configuration',
    '#collapsible' => false,
  );
  $form['fb_block']['fb_bl_layout'] = array(
    '#type' => 'select',
    '#title' => t('Layout style'),
    '#options' => array('standard' => t('Standard'), 'box_count' => t('Box Count'), 'button_count' => t('Button Count')),
    '#default_value' => variable_get('fb_bl_layout', 'standard'),
    '#description' => t('Determines the size and amount of social context next to the button'),
  );
  $form['fb_block']['fb_bl_show_faces'] = array(
    '#type' => 'select',
    '#title' => t('Display faces in the box'),
    '#options' => array('show' => t('Show faces'), 'hide' => t('Do not show faces')),
    '#default_value' => variable_get('fb_bl_show_faces', 'show'),
    '#description' => t('Show profile pictures below the button. Only works with Standard layout'),
  );
  $form['fb_block']['fb_bl_action'] = array(
    '#type' => 'select',
    '#title' => t('Verb to display'),
    '#options' => array('like' => t('Like'), 'recommend' => t('Recommend')),
    '#default_value' => variable_get('fb_bl_action', 'like'),
    '#description' => t('The verb to display in the button.'),
  );
  $form['fb_block']['fb_bl_font'] = array(
      '#type' => 'select',
      '#title' => t('Font'),
      '#options' => array('arial' => 'Arial',
                'lucida+grande' => 'Lucida Grande',
                'segoe+ui' => 'Segoe UI',
                'tahoma' => 'Tahoma',
                'trebuchet+ms' => 'Trebuchet MS',
                'verdana' => 'Verdana'),
      '#default_value' => variable_get('fb_bl_font', 'arial'),
      '#description' => t('The font to display in the button'),
    );

  $form['fb_block']['fb_bl_color_scheme'] = array(
    '#type' => 'select',
    '#title' => t('Color scheme'),
    '#options' => array('light' => t('Light'), 'dark' => t('Dark')),
    '#default_value' => variable_get('fb_bl_color_scheme', 'light'),
    '#description' => t('The color scheme of box environtment'),
  );
  $form['fb_block']['fb_bl_iframe_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width of the iframe (px)'),
    '#default_value' => variable_get('fb_bl_iframe_width', '450'),
    '#description' => t('Width of the iframe, in pixels. Default is 450. <em>Note: lower values may crop the output.</em>'),
  );
  $form['fb_block']['fb_bl_iframe_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height of the iframe (px)'),
    '#default_value' => variable_get('fb_bl_iframe_height', '80'),
    '#description' => t('Height of the iframe, in pixels. Default is 80. <em>Note: lower values may crop the output.</em>'),
  );
  $form['fb_block']['fb_bl_iframe_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra css styling needed'),
    '#default_value' => variable_get('fb_bl_iframe_css', ''),
    '#description' => t('Extra css attributes needed to make the iframe behave for your specific requirements. Will not necessarily overwrite existing styling. To alter the dimensions of the iframe, use the height and width fields found above.<br/>Example: <em>float: right; padding: 5px;</em>'),
  );
  $form['fb_block']['fb_bl_language'] = array(
    '#type' => 'textfield',
    '#title' => t('Language'),
    '#default_value' => variable_get('fb_language', 'en_US'),
    '#description' => t('Specific language to use. Default is English. Examples:<br/>French (France): <em>fr_FR</em><br/>French (Canada): <em>fr_CA</em>'),
  );

  return system_settings_form($form);
}

*/
